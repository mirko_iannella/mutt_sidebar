# Note: Mutt has a large number of non-upstream patches available for
# it, some of which conflict with each other. These patches are also
# not kept up-to-date when new versions of mutt (occasionally) come
# out.
#
# To reduce Homebrew's maintenance burden, new patches are not being
# accepted for this formula. We would be very happy to see members of
# the mutt community maintain a more comprehesive tap with better
# support for patches.

class Mutt < Formula
  desc "Mongrel of mail user agents (part elm, pine, mush, mh, etc.)"
  homepage "http://www.mutt.org/"
  url "https://bitbucket.org/mutt/mutt/downloads/mutt-1.5.24.tar.gz"
  mirror "ftp://ftp.mutt.org/pub/mutt/mutt-1.5.24.tar.gz"
  sha256 "a292ca765ed7b19db4ac495938a3ef808a16193b7d623d65562bb8feb2b42200"

  bottle do
    sha256 "9d83e71eeca14f5494a07abd68b6a723928cf415157dbf070461a10d0a0d89ae" => :yosemite
    sha256 "28b3aa2d69d4eb12da355f7639c3e7eb4124337ff0c0d91477b4dd75c161ac67" => :mavericks
    sha256 "3ed3daff645991c2f4a7f3eb91b6f65facced496e4d1aa28584f1cad29081763" => :mountain_lion
  end

  head do
    url "http://dev.mutt.org/hg/mutt#default", :using => :hg

    resource "html" do
      url "http://dev.mutt.org/doc/manual.html", :using => :nounzip
    end
  end

  unless Tab.for_name("signing-party").with? "rename-pgpring"
    conflicts_with "signing-party",
      :because => "mutt installs a private copy of pgpring"
  end

  conflicts_with "tin",
    :because => "both install mmdf.5 and mbox.5 man pages"

  option "with-debug", "Build with debug option enabled"
  option "with-s-lang", "Build against slang instead of ncurses"
  option "with-ignore-thread-patch", "Apply ignore-thread patch"
  option "with-confirm-attachment-patch", "Apply confirm attachment patch"
  option "with-sidebar-patch", "Apply sidebar patch"

  depends_on "autoconf" => :build
  depends_on "automake" => :build

  depends_on "openssl"
  depends_on "tokyo-cabinet"
  depends_on "s-lang" => :optional
  depends_on "gpgme" => :optional

  # original source for this went missing, patch sourced from Arch at
  # https://aur.archlinux.org/packages/mutt-ignore-thread/
  if build.with? "ignore-thread-patch"
    patch do
      url "https://gist.githubusercontent.com/mistydemeo/5522742/raw/1439cc157ab673dc8061784829eea267cd736624/ignore-thread-1.5.21.patch"
      sha256 "7290e2a5ac12cbf89d615efa38c1ada3b454cb642ecaf520c26e47e7a1c926be"
    end
  end

  if build.with? "confirm-attachment-patch"
    patch do
      url "https://gist.githubusercontent.com/tlvince/5741641/raw/c926ca307dc97727c2bd88a84dcb0d7ac3bb4bf5/mutt-attach.patch"
      sha256 "da2c9e54a5426019b84837faef18cc51e174108f07dc7ec15968ca732880cb14"
    end
  end

  if build.with? "sidebar-patch"
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/trash-folder.patch"
      sha256 "bec56cd374735dfd2ccd05f606f3b0e4fa70ac0403bdc24d802cb3c98c1ddf40"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar.patch"
      sha256 "fefc1d12ac6006c4ad53a7a3893170ae312ae8dabbc913bd15a8906a9f9396bf"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-dotpathsep.patch"
      sha256 "e31c6be587087314f59d11235b95e03f3aa095f70c64d33b47d7d510466c0581"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-utf8.patch"
      sha256 "998fa2c73aa50c495071c019c2616f4b7fd1f8ef7bc44ee08bad5af76a69d776"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-newonly.patch"
      sha256 "afd43eb47b27dd820c2195ac18cfe6421dcf0bb74188881521adbe9a8216efee"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-delimnullwide.patch"
      sha256 "3fdae2e6db9b223fd30d24faf52dc0f9a9377cf75291533ee1e36c46fdcb314c"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-compose.patch"
      sha256 "9d08dc2ca9aab3697273d6f77bce9d0ed701e066aeb42e1e4f15ded3b888c20b"
    end
    patch do
      url "https://bitbucket.org/mirko_iannella/mutt_sidebar/raw/9e217c616b1ef1245667c7b7f93a91154e13b3f1/sidebar-new.patch"
      sha256 "6c8345d67696145fa93d414ef6e514f577d5edbd2ba12369cde34198bdeb2edb"
    end
  end

  def install
    args = ["--disable-dependency-tracking",
            "--disable-warnings",
            "--prefix=#{prefix}",
            "--with-ssl=#{Formula["openssl"].opt_prefix}",
            "--with-sasl",
            "--with-gss",
            "--enable-imap",
            "--enable-smtp",
            "--enable-pop",
            "--enable-hcache",
            "--with-tokyocabinet",
            # This is just a trick to keep 'make install' from trying
            # to chgrp the mutt_dotlock file (which we can't do if
            # we're running as an unprivileged user)
            "--with-homespool=.mbox"]
    args << "--with-slang" if build.with? "s-lang"
    args << "--enable-gpgme" if build.with? "gpgme"

    if build.with? "debug"
      args << "--enable-debug"
    else
      args << "--disable-debug"
    end

    system "./prepare", *args
    system "make"
    system "make", "install"

    doc.install resource("html") if build.head?
  end

  test do
    system bin/"mutt", "-D"
  end
end
